package com.test.kmeans;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class Container {
	private static final String PATH = "C:/Users/Artsiom_Krauchanka/drob.jpg";
	private static final String FINAL_PATH = "C:/Users/Artsiom_Krauchanka/drob1.jpg";

	private List<Pixel> pixels = new ArrayList<Pixel>();
	private List<Pixel> kernels = new ArrayList<Pixel>();
	private BufferedImage image;

	public Container() throws IOException {
		int id = 0;
		image = ImageIO.read(new File(PATH));
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Pixel pixel = new Pixel();
				pixel.setId(++id);
				pixel.setX(i);
				pixel.setY(j);
				pixel.setColor(new Color(image.getRGB(i, j)));
				pixel.setCls(-1);
				pixels.add(pixel);
			}
		}
	}

	public List<Pixel> getPixels() {
		return pixels;
	}

	public BufferedImage getImage() {
		return image;
	}

	public List<Pixel> getKernels() {
		return kernels;
	}

	public void setKernels(List<Pixel> kernels) {
		this.kernels = kernels;
	}

	public Pixel getById(int id) {
		return (Pixel) pixels.stream().filter(p -> p.getId() == id).findFirst().get();
	}

	public Pixel getByXY(int x, int y) {
		return (Pixel) pixels.stream().filter(p -> p.getX() == x && p.getY() == y).findFirst().get();
	}

	public void setNewImage() {
		for (int i = 0; i < image.getWidth(); i++) {
			for (int j = 0; j < image.getHeight(); j++) {
				Pixel pixel = getByXY(i, j);
				int rgb = ((Pixel) kernels.stream().filter(p -> p.getCls() == pixel.getCls()).findFirst().get()).getColor().getRGB();
				image.setRGB(i, j, rgb);
			}
		}
		File outputfile = new File(FINAL_PATH);
		try {
			ImageIO.write(image, "jpg", outputfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public long getCountOfClass(int cls) {
		return pixels.stream().filter(p -> p.getCls() == cls).count();
	}
}
