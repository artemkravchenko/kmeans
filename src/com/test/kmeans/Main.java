package com.test.kmeans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	private static final int CLS_COUNT = 6;

	public Container container;

	public void execute() {
		try {
			container = new Container();
		} catch (IOException e) {
			return;
		}

		for (int i = 0; i < CLS_COUNT;) {
			int id = new Random().nextInt(container.getImage().getWidth() * container.getImage().getHeight());
			Pixel pixel = container.getById(id);
			if (pixel.getCls() != -1) {
				continue;
			}
			pixel.setCls(i++);
			container.getKernels().add(pixel);
		}

		boolean clustering = true;
		long[] averageColor = null;
		while (clustering) {
			averageColor = new long[CLS_COUNT];

			for (Pixel p : container.getPixels()) {
				if (container.getKernels().contains(p)) {
					continue;
				}
				double mindist = Double.MAX_VALUE;
				int cls = -1;
				for (Pixel k : container.getKernels()) {
					double dist = getDistance(p.getColor().getRGB(), k.getColor().getRGB());
					if (dist < mindist) {
						mindist = dist;
						cls = k.getCls();
					}
				}
				p.setCls(cls);
				averageColor[cls] = averageColor[cls] + p.getColor().getRGB();
			}

			for (Pixel p : container.getKernels()) {
				averageColor[p.getCls()] += p.getColor().getRGB();
			}

			clustering = false;
			List<Pixel> newKernels = new ArrayList<Pixel>();

			for (int j = 0; j < CLS_COUNT; j++) {

				int averageRGB = (int) (averageColor[j] / container.getCountOfClass(j));
				int minDiff = Integer.MAX_VALUE;
				int candId = -1;

				for (Pixel p : container.getPixels()) {
					if (p.getCls() != j) {
						continue;
					}

					int dist = getDistance(p.getColor().getRGB(), averageRGB);
					if (dist < minDiff) {
						minDiff = dist;
						candId = p.getId();
					}

				}
				int cls = j;
				Pixel oldKernel = (Pixel) container.getKernels().stream().filter(p -> p.getCls() == cls).findFirst().get();
				if (oldKernel.getId() != candId) {
					clustering = true;
					newKernels.add(container.getById(candId));
				} else {
					newKernels.add(oldKernel);
				}
			}
			container.setKernels(newKernels);

		}
		container.setNewImage();
	}

	public static int getDistance(int rgb1, int rgb2) {
		return Math.abs(rgb1 - rgb2);
	}

	public static void main(String[] args) {
		new Main().execute();
	}
}
